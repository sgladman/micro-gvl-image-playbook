#This is a sample file distributed with Galaxy that enables tools
#to use a directory of Samtools indexed sequences data files.  You will need
#to create these data files and then create a sam_fa_new_indices.loc file
#similar to this one (store it in this directory) that points to
#the directories in which those files are stored. The sam_fa_new_indices.loc
#file has this format (white space characters are TAB characters):
#
# <unique_build_id>	<dbkey>	<display_name>	<file_base_path>
#
#So, for example, if you had hg19 Canonical indexed stored in
#
# /depot/data2/galaxy/hg19/sam/,
#
#then the sam_fa_new_indices.loc entry would look like this:
#
#hg19canon	hg19	Human (Homo sapiens): hg19 Canonical	/depot/data2/galaxy/hg19/sam/hg19canon.fa
#
#and your /depot/data2/galaxy/hg19/sam/ directory
#would contain hg19canon.fa and hg19canon.fa.fai files.
#
#Your sam_fa_new_indices.loc file should include an entry per line for
#each index set you have stored.  The file in the path does actually
#exist, but it should never be directly used. Instead, the name serves
#as a prefix for the index file.  For example:
#
#hg18canon	hg18	Human (Homo sapiens): hg18 Canonical	/depot/data2/galaxy/hg18/sam/hg18canon.fa
#hg18full	hg18	Human (Homo sapiens): hg18 Full	/depot/data2/galaxy/hg18/sam/hg18full.fa
#hg19canon	hg19	Human (Homo sapiens): hg19 Canonical	/depot/data2/galaxy/hg19/sam/hg19canon.fa
#hg19full	hg19	Human (Homo sapiens): hg19 Full	/depot/data2/galaxy/hg19/sam/hg19full.fa
dm3	dm3	D melanogaster (dm3)	/mnt/galaxyIndices/genomes/dm3/sam_index/dm3.fa
araTha_tair9	araTha_tair9	Arabidopsis thaliana (TAIR9)	/mnt/galaxyIndices/genomes/Arabidopsis_thaliana_TAIR9/sam_index/Arabidopsis_thaliana_TAIR9.fa
phix	phix	phiX174	/mnt/galaxyIndices/genomes/phiX/sam_index/phiX.fa
ce10	ce10	C elegans (ce10)	/mnt/galaxyIndices/genomes/ce10/sam_index/ce10.fa
danRer7	danRer7	Zebrafish (danRer7)	/mnt/galaxyIndices/genomes/danRer7/sam_index/danRer7.fa
hg19	hg19	Human (hg19)	/mnt/galaxyIndices/genomes/hg19/sam_index/hg19.fa
hg_g1k_v37	hg_g1k_v37	Human (hg_g1k_v37)	/mnt/galaxyIndices/genomes/hg_g1k_v37/sam_index/hg_g1k_v37.fa
hg38	hg38	Human (hg38)	/mnt/galaxyIndices/genomes/hg38/sam_index/hg38.fa
mm10	mm10	Mouse (mm10)	/mnt/galaxyIndices/genomes/mm10/sam_index/mm10.fa
mm9	mm9	Mouse (mm9)	/mnt/galaxyIndices/genomes/mm9/sam_index/mm9.fa
rn5	rn5	Rat (rn5)	/mnt/galaxyIndices/genomes/rn5/sam_index/rn5.fa
rn6	rn6	Rat (rn6)	/mnt/galaxyIndices/genomes/rn6/sam_index/rn6.fa
sacCer2	sacCer2	Saccharomyces cerevisiae (sacCer2)	/mnt/galaxyIndices/genomes/sacCer2/sam_index/sacCer2.fa
sacCer3	sacCer3	Saccharomyces cerevisiae (sacCer3)	/mnt/galaxyIndices/genomes/sacCer3/sam_index/sacCer3.fa
bosTau7	bosTau7	Cow (bosTau7)	/mnt/galaxyIndices/genomes/bosTau7/sam_index/bosTau7.fa
oviAri3	oviAri3	Sheep (oviAri3)	/mnt/galaxyIndices/genomes/oviAri3/sam_index/oviAri3.fa
xenTro2	xenTro2	X tropicalis (xenTro2)	/mnt/galaxyIndices/genomes/xenTro2/sam_index/xenTro2.fa
